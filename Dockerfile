FROM ubuntu:20.04

RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 5000

VOLUME ["/app/images", "/app/api/database"]

ENTRYPOINT [ "python3" ]

CMD [ "run.py" ]