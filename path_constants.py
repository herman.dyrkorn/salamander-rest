# This file contains consts to path and the program
# uses these for accessing.

import os.path as os
from threading import Semaphore

# The below method shouldn't be used since this code may run on a windows or linux in the future:
# abs_path_work_dir = os.getcwd()
# abs_path_dlc_model = os.getcwd() + "\\algorithm\\train_src\\dlc_model"
# abs_path_dlc_config = abs_path_dlc_model + "\\config.yaml"
# abs_path_img_analyze = os.getcwd() + "\\img_analyze"

abs_path_work_dir = os.abspath("")

imageai_model_name = "detection_model-ex-012--loss-0006.916.h5"
imageai_config_name = "detection_config.json"

abs_path_temp_images = os.abspath("temp_images")
abs_path_images = os.abspath("images")

# model:
abs_path_dlc_model = os.abspath("algorithm/dlc_model")
abs_path_dlc_config = os.abspath("algorithm/dlc_model/config.yaml")
abs_path_imageai_model = os.abspath("algorithm/imageai_model/models/" + imageai_model_name)
abs_path_imageai_config = os.abspath("algorithm/imageai_model/json/" + imageai_config_name)


image_type = "jpg"

_ACCESS_DATABASE = Semaphore(1)