import os
from cv2 import cv2
import glob
from algorithm.SalamanderImage import SalamanderImage


def match_single_image(input_salamander, match_salamander):
    """
    Compares two SalamanderImages and determines if they are similar enough to be a match

    Args:
        input_salamander: SalamanderImage of input salamander
        match_salamander: SalamanderImage of salamander from the database

    Returns:
        Boolean value indicating if the comparison was a match and the number of hits gotten in the comparison
    """

    min_good_match = 15
    match_dist = 0.75

    match = cv2.BFMatcher().knnMatch(input_salamander.descriptors, match_salamander.descriptors, k=2)

    goodmatch = []
    for m, n in match:
        if m.distance < match_dist * n.distance:
            goodmatch.append([m])
    if len(goodmatch) > min_good_match:
        return True, len(goodmatch)
    return False, 0


def match_file_structure(input_image: str, match_directory: str):
    """
    Loops through a given directory of salamanders represented by folders containing their images, and finds the best
    match (if any) based on an input image

    Args:
        input_image: SalamanderImage of input salamander
        match_directory: Path of directory to looped through

    Returns:
        The ID of the best matched salamander or None if no match is found
    """

    best_match = -1
    match_count = 0

    # check if input path is valid:
    if os.path.isfile(input_image):
        input_salamander = SalamanderImage(input_image)
        for folder in os.listdir(match_directory):
            name_list = glob.glob(os.path.join(match_directory, folder, "*_str.*"))
            for filename in name_list:
                res, num_matches = match_single_image(input_salamander, SalamanderImage(filename))
                if res and num_matches > match_count:
                    match_count = num_matches
                    best_match = int(folder)
        return best_match
    else:
        return None
