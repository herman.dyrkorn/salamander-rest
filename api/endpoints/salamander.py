from flask import jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from api import db, limiter
from api.models.dbmodels import Location, User, Salamander
import os
import glob
import numpy as np
from cv2 import cv2
from image_encoder.image_encoder import *
from path_constants import _ACCESS_DATABASE


class SalamanderEndpoint(Resource):
    """
    Endpoint for getting all original images of a specific salamander.
    GET: Gets all the images based on a salamanders ID.
    """

    decorators = [limiter.limit("60/minute")]

    @staticmethod
    @jwt_required
    def get(salamander_id):
        """
        :param salamander_id: the id of a specific salamander
        :return: JSON object containing all original images and data about a specific salamander
        """
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            with _ACCESS_DATABASE:
                salamander = db.session.query(Salamander).filter_by(id=salamander_id).first()
                if salamander:
                    location = db.session.query(Location).filter_by(id=salamander.location_id).first()
                    salamander_images = []
                    path_to_salamander_images = os.path.join("./images", location.name, salamander.species, salamander.sex, str(salamander.id))
                    list_of_paths = glob.glob(os.path.join(path_to_salamander_images, '*.*'))
                    for path in list_of_paths:
                        if not path.__contains__("_str"):
                            image = cv2.imdecode(np.fromfile(path, dtype=np.uint8), cv2.IMREAD_UNCHANGED)

                            if image is None:
                                raise FileNotFoundError("Cannot find image file " + path)

                            # scaling to set size
                            height, width, _ = image.shape

                            desired_long_side = 320

                            if width > height:
                                scaling_factor = desired_long_side / width
                            else:
                                scaling_factor = desired_long_side / height

                            new_dim = (int(width * scaling_factor), int(height * scaling_factor))

                            # resize image
                            image = cv2.resize(image, new_dim, interpolation=cv2.INTER_AREA)

                            _, buffer = cv2.imencode('.jpg', image)
                            encoded = base64.b64encode(buffer)

                            image_id = os.path.basename(path)[0]
                            image_data = {"id": image_id, "url": "data:image/png;base64," + encoded.decode()}
                            salamander_images.append(image_data)

                    salamander_images.sort(key=lambda x: x.get('id'))
                    return jsonify({"images": salamander_images, "location": location.name, "id": salamander.id, "sex": salamander.sex, "species": salamander.species, 'status': 200})
                else:
                    return jsonify({"message": "no salamander with this id", 'status': 400})
        else:
            return jsonify({"message": "no access to this endpoint", 'status': 400})
