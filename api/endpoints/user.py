from flask import request, jsonify
from flask_restful import Resource
from api.models.dbmodels import User
from api import bcrypt, db, limiter
import re
from flask_jwt_extended import get_jwt_identity, jwt_required
from api.forms.userforms import RegistrationForm, DeleteUserForm

EMAIL_REGEX = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"


class UserEndpoint(Resource):
    """
    Endpoint for creating a new user, deleting a user and edit a user.
    POST: For creating a new user with email, name, password and confirm password.
    PUT: For editing a user. Either edit name, email or password.
    DELETE: For deleting a user. An administrator cannot delete their own account.
    """

    decorators = [limiter.limit("30/minute")]

    @staticmethod
    def post():
        """
        param email: the registered users email
        param name: the registered users name
        param password: the registered users password
        param confirmPassword: the registered users password confirmed
        :return: JSON object that contains a confirmation message
        """
        data = RegistrationForm(request.form)
        if data.validate():
            if data.password.data == data.confirmPassword.data:
                user = db.session.query(User).filter_by(email=data.email.data.lower()).first()
                if not user and re.search(EMAIL_REGEX, data.email.data.lower()):
                    password_hash = bcrypt.generate_password_hash(data.password.data)
                    new_user = User(name=data.name.data, email=data.email.data.lower(), pwd=password_hash)
                    db.session.add(new_user)
                    db.session.commit()
                    return jsonify({"message": "new user created", 'status': 200})
                else:
                    return jsonify({"message": "invalid email", 'status': 400})
            else:
                return jsonify({"message": "password does not match", 'status': 400})
        else:
            return jsonify({"message": "wrong data posted", 'status': 400})

    @staticmethod
    @jwt_required
    def delete():
        """
        param email: the users email
        :return: JSON object that contains a confirmation message
        """
        data = DeleteUserForm(request.form)
        user_id = get_jwt_identity()
        if data.validate():
            user = db.session.query(User).filter_by(email=data.email.data.lower()).first()
            if not user.admin:
                if user and user_id == user.id:
                    db.session.delete(user)
                    db.session.commit()
                    return jsonify({"message": "user deleted", 'status': 200})
                else:
                    return jsonify({"message": "email or password not matching", 'status': 400})
            else:
                return jsonify({"message": "admins cannot delete themselves", 'status': 400})
        else:
            return jsonify({"message": "wrong data sent", 'status': 400})

    @staticmethod
    @jwt_required
    def put():
        """
        param newName: the users new name
        param newEmail: the users new email
        param newPassword: the users new password
        param confirmNewPassword: the users new password confirmed
        :return: JSON object that contains a confirmation message
        """
        data = request.form
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user is not None:
            if 'newName' in data:
                user.name = data['newName']
                db.session.commit()
                return jsonify({"message": "name was updated", "status": 200})

            if 'newEmail' in data:
                user_check = db.session.query(User).filter_by(email=data['newEmail']).first()
                if user_check is None and re.search(EMAIL_REGEX, data['newEmail'].lower()):
                    user.email = data['newEmail'].lower()
                    db.session.commit()
                    return jsonify({"message": "email was updated", "status": 200})
                else:
                    return jsonify({"message": "invalid email", "status": 400})

            if 'newPassword' in data and 'confirmNewPassword' in data:
                if data['newPassword'] == data['confirmNewPassword']:
                    password_hash = bcrypt.generate_password_hash(data['newPassword'])
                    user.pwd = password_hash
                    db.session.commit()
                    return jsonify({"message": "password was updated", "status": 200})
                else:
                    return jsonify({"message": "password does not match", "status": 400})

        else:
            return jsonify({"message": "this user does not exist", "status": 400})
