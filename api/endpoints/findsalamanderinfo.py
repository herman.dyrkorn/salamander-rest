from flask import request, jsonify
from flask_restful import Resource
from flask_jwt_extended import get_jwt_identity, jwt_required
import os
import requests
from api import limiter
import cv2
import numpy as np
from image_encoder.image_encoder import *
import glob

from path_constants import image_type

ALLOWED_EXTENSIONS = ['jpg', 'png', 'jpeg']


class FindSalamanderInfo(Resource):
    """
    Endpoint for passing an image to the server for processing the salamander abdominal pattern.
    POST: Receives an image of a salamander, sends it to the processing server, gets the returned abdominal pattern,
    stores it in the temp_images folder, and returns the processed image back to the user.
    """

    decorators = [limiter.limit("5/minute")]

    @staticmethod
    @jwt_required
    def post():
        """
        param image: an image file of a salamander
        :return: JSON object containing the processed image or error messages
        """
        if "image" not in request.files:
            return jsonify({"message": "no file given", "status": 400})
        image = request.files['image']
        user_id = get_jwt_identity()
        if image.filename != '':
            if image and allowed_image(image.filename):
                temp_img_path = os.path.abspath("./temp_images/")
                list_of_temp_images = glob.glob(os.path.join(temp_img_path, str(user_id) + '*.*'))
                for temp_image in list_of_temp_images:
                    os.remove(temp_image)

                image.filename = str(user_id) + "." + extension(image.filename)
                image.save(os.path.join(temp_img_path, image.filename))

                encoded = encode(os.path.join(temp_img_path, str(user_id) + "." + extension(image.filename)))[2:-1]

                send_to_server = {'image': encoded}

                response = requests.post('http://0.0.0.0:4500/findSalamanderInfo', json=send_to_server)

                response_json = response.json()

                if response_json['status'] == 200:

                    str_image_base64 = response_json['image']

                    nparr = np.fromstring(base64.b64decode(str_image_base64), np.uint8)

                    str_image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

                    cv2.imwrite(os.path.join(temp_img_path, str(user_id) + "_str." + image_type), str_image)

                    return jsonify({"status": 200, "image": response_json['image']})
                else:
                    return jsonify({"message": "image could not be processed", "status": 400})
            else:
                return jsonify({"message": "file extension not allowed", "status": 400})
        else:
            return jsonify({"message": "something wrong with image", "status": 400})


def allowed_image(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def extension(filename):
    return filename.rsplit('.', 1)[1].lower()
