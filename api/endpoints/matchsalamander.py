from flask import request, jsonify
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import os.path
from shutil import move
from re import sub
from api import db, limiter
from api.models.dbmodels import Salamander, SalamanderGrowth, Location
from algorithm.brute_force_matching import match_file_structure
from path_constants import _ACCESS_DATABASE
import glob

APPROVED_SEX = ["male", "female", "juvenile"]
APPROVED_SPECIES = ["smooth_newt", "northern_crested_newt"]


# matchSalamander endpoint
class MatchSalamander(Resource):
    """
    Endpoint for matching a newly registered salamander with the once in the database.
    POST: Matched the processed image of the salamander pattern with the once in the database.
    It only matches females and males, not juveniles.
    """

    decorators = [limiter.limit("5/minute")]

    @staticmethod
    @jwt_required
    def post():
        """
        param sex: the sex of the salamander
        param location: the location name for salamander registration
        param species: the species of the salamander
        :return: JSON object that contains match, no match or error message
        """
        data = request.form
        user_id = get_jwt_identity()
        approved_locations = get_locations()

        if "sex" in data and "location" in data and "species" in data:
            if data['sex'] in APPROVED_SEX and data['species'] in APPROVED_SPECIES and data['location'].lower() in approved_locations:
                with _ACCESS_DATABASE:
                    temp_img_path = os.path.abspath("./temp_images/")
                    img_path = os.path.abspath("./images/")
                    list_of_name = glob.glob(os.path.join(temp_img_path, str(user_id) + '.*'))
                    list_of_name_str = glob.glob(os.path.join(temp_img_path, str(user_id) + '_str.*'))
                    if len(list_of_name_str) > 0 and len(list_of_name) > 0:
                        img_name = list_of_name[0]
                        img_str_name = list_of_name_str[0]
                        path_to_processed_image = os.path.join(temp_img_path, img_str_name)
                        path_to_original_image = os.path.join(temp_img_path, img_name)
                        path_to_images = os.path.join(img_path, data['location'].lower(), data['species'], data['sex'])
                        weight = None
                        length = None
                        if "weight" in data:
                            weight = sanitize_number_str(str(data['weight']))
                        if "length" in data:
                            length = sanitize_number_str(str(data['length']))
                        if data['sex'] != "juvenile":
                            result = match_file_structure(path_to_processed_image, path_to_images)
                            if result:
                                if result > -1:
                                    number_of_files = int(len(os.listdir(os.path.join(path_to_images, str(result)))) / 2)
                                    add_salamander_growth(weight, length, result, number_of_files)
                                    move_and_rename_images(os.path.join(path_to_images, str(result)), path_to_original_image, path_to_processed_image)
                                    return jsonify({"matching": "Match!", "message": "Matched with salamander", "id": result, 'status': 200})
                                else:
                                    salamander_id = add_salamander(data['location'], data['species'], data['sex'], path_to_images, user_id, path_to_original_image, path_to_processed_image)
                                    number_of_files = int(len(os.listdir(os.path.join(path_to_images, str(salamander_id)))) / 2) - 1
                                    add_salamander_growth(weight, length, salamander_id, number_of_files)
                                    return jsonify({"matching": "No Match.", "message": "New salamander in database", "id": salamander_id, 'status': 200})
                            else:
                                return jsonify({"message": "Internal server error", 'status': 500})
                        else:
                            salamander_id = add_salamander(data['location'], data['species'], data['sex'], path_to_images, user_id, path_to_original_image, path_to_processed_image)
                            number_of_files = int(len(os.listdir(os.path.join(path_to_images, str(salamander_id)))) / 2) - 1
                            add_salamander_growth(weight, length, salamander_id, number_of_files)
                            return jsonify({"matching": "No Match.", "message": "New juvenile in database", "id": salamander_id, 'status': 200})
                    else:
                        return jsonify({"message": "internal server error", 'status': 500})
            else:
                return jsonify({"message": "invalid sex, species or location", 'status': 400})
        else:
            return jsonify({"message": "wrong data sent", 'status': 400})


def add_salamander(location, species, sex, path_to_images, user_id, path_to_original_image, path_to_processed_image):
    location_id = db.session.query(Location.id).filter_by(name=location.lower()).first()
    new_salamander = Salamander(sex=sex, species=species, location_id=location_id[0], uid=user_id)
    db.session.add(new_salamander)
    db.session.commit()
    salamander_id = db.session.query(Salamander.id).filter_by(uid=user_id).order_by(Salamander.id.desc()).first()
    path_for_new_salamander = os.path.join(path_to_images, str(salamander_id[0]))
    os.makedirs(path_for_new_salamander)
    move_and_rename_images(path_for_new_salamander, path_to_original_image, path_to_processed_image)
    return salamander_id[0]


def move_and_rename_images(path_to_salamander: str = "", path_to_original_image: str = "", path_to_processed_image: str = ""):
    path_to_salamander = os.path.abspath(path_to_salamander)
    split_name_original = str(path_to_original_image).split('.')
    split_name_processed = str(path_to_processed_image).split('.')
    number_of_files = int(len(os.listdir(path_to_salamander)) / 2)
    a = os.path.join(path_to_salamander, str(number_of_files)) + "." + split_name_original[len(split_name_original)-1]
    b = os.path.join(path_to_salamander, str(number_of_files)) + "_str." + split_name_processed[len(split_name_original)-1]
    move(path_to_original_image, a)
    move(path_to_processed_image, b)


def add_salamander_growth(weight, length, salamander_id, image_id):
    salamander_growth = SalamanderGrowth(salamander_id=salamander_id, image_id=image_id, weight=weight, length=length)
    db.session.add(salamander_growth)
    db.session.commit()


def get_locations():
    locations = db.session.query(Location.name).all()
    approved_locations = []
    for location in locations:
        approved_locations.append(location.name)
    return approved_locations


def sanitize_number_str(string_num: str = None):
    if string_num is None:
        return None
    if string_num == "":
        return None
    string_num = string_num.replace(',', '.')
    string_num = sub("[^\d.]", '', string_num)
    string_list = string_num.split('.')
    # if string_list is greater that 2 the rest is ignored:
    if len(string_list) > 1:
        if string_list[0] == "":
            string_list[0] = '0'
        if string_list[1] != "":
            string_num = string_list[0] + '.' + string_list[1]
    return float(string_num)


def sanitize_int_str(string_num: str = None):
    if string_num is None:
        return None
    if string_num == "":
        return None
    string_num = sub("[^\d]", '', string_num)
    # if string_list is greater that 2 the rest is ignored:
    return int(string_num)
