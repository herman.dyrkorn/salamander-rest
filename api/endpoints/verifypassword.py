from flask import request, jsonify
from flask_restful import Resource
from api import bcrypt, db, limiter
from flask_jwt_extended import get_jwt_identity, jwt_required
from api.models.dbmodels import User


class VerifyPassword(Resource):
    """
    Endpoint to verify a users password.
    POST: Checks that the password that the user typed in is correct.
    """

    decorators = [limiter.limit("30/minute")]

    @staticmethod
    @jwt_required
    def post():
        """
        param password: the users password
        :return: JSON object containing if the user is verified or not
        """
        data = request.form
        if "password" in data:
            user_id = get_jwt_identity()
            user = db.session.query(User).filter_by(id=user_id).first()
            if user is not None:
                if user.id == user_id:
                    if bcrypt.check_password_hash(user.pwd, data['password']):
                        return jsonify({"verified": True, "status": 200})
                    else:
                        return jsonify({"verified": False, "status": 400})
                else:
                    return jsonify({"verified": False, "status": 400})
            else:
                return jsonify({"verified": False, "status": 400})
        else:
            return jsonify({"verified": False, "status": 400})
