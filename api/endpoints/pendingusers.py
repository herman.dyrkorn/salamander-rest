from flask import request, jsonify
from flask_restful import Resource
from api.models.dbmodels import User
from api import db, limiter
from flask_jwt_extended import get_jwt_identity, jwt_required


class AdminPendingUsers(Resource):
    """
    Endpoint for administrators to accept newly created users.
    GET: Gets all pending users in the system.
    POST: For either accepting or denying a users access permissions. If the user is not accepted, it will be deleted.
    """

    decorators = [limiter.limit("60/minute")]

    @staticmethod
    @jwt_required
    def get():
        """
        :return: JSON object containing all users that are not approved by an admin
        """
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            user_list = []
            users = db.session.query(User).filter_by(accepted=0).all()
            for pr_user in users:
                ret = {"email": pr_user.email, "id": pr_user.id}
                user_list.append(ret)
            return jsonify({"users": user_list, "status": 200})
        else:
            return jsonify({"message": "no access", "status": 400})

    @staticmethod
    @jwt_required
    def post():
        """
        param id: the user id for the user that is being approved
        param email: the user email for the user that is being approved
        param accepted: 1 if the user is accepted, 0 it the user is not accepted
        :return: JSON object containing a confirmation message
        """
        data = request.form
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            if "id" in data and "email" in data and "accepted" in data:
                update_user = db.session.query(User).filter_by(id=data['id']).first()
                if update_user and data['accepted'] == "1" and update_user.email == data['email']:
                    update_user.accepted = True
                    db.session.commit()
                    return jsonify({"message": "user approved", "status": 200})

                elif update_user and data['accepted'] == "0" and update_user.email == data['email']:
                    db.session.delete(update_user)
                    db.session.commit()
                    return jsonify({"message": "user deleted", "status": 200})
                else:
                    return jsonify({"message": "user does not exist", "status": 400})

            else:
                return jsonify({"message": "wrong data", "status": 400})
        else:
            return jsonify({"message": "no access", "status": 400})
