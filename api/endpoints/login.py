from flask import request, jsonify
from flask_jwt_extended import create_access_token
from flask_restful import Resource
from api.models.dbmodels import User
from api import bcrypt, db, limiter


class Login(Resource):
    """
    Endpoint for logging in to the system.
    POST: Checks if the email and password matches. Returns a JSON Web Token to the user that requested the login.
    This JWT contains the users ID encrypted in it, so the server knows who sends request to other endpoints.
    """

    decorators = [limiter.limit("3/minute")]

    @staticmethod
    def post():
        """
        param email: the users email
        param password: the users password
        :return: JSON object containing user data and an access token(JWT), or an error message
        """
        data = request.form
        if data['email'] or data['password']:
            user = db.session.query(User).filter_by(email=data['email'].lower()).first()
            if user and bcrypt.check_password_hash(user.pwd, data['password']):
                if user.accepted:
                    ret = {
                        'access_token': create_access_token(identity=user.id),
                        'message': "successfully logged in",
                        'admin': user.admin,
                        'name': user.name,
                        'email': user.email,
                        'status': 200
                    }
                    return jsonify(ret)
                else:
                    return jsonify({'message': 'not accepted by admin', 'status': 400})
            else:
                return jsonify({'message': 'login failed, wrong email or password', 'status': 400})
        else:
            return jsonify({"message": "data provided was incorrect", 'status': 400})
