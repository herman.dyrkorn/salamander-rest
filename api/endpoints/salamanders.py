from flask import jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from api import db, limiter
from api.models.dbmodels import Location, User, Salamander, SalamanderGrowth
from path_constants import _ACCESS_DATABASE


class SalamandersEndpoint(Resource):
    """
    Endpoint for for getting data about all salamanders in a specific location.
    GET: Gets all salamanders and their data based on a specific location.
    """

    decorators = [limiter.limit("60/minute")]

    @staticmethod
    @jwt_required
    def get(location_name):
        """
        :param location_name: name of the location
        :return: JSON object containing a list of all salamanders and its data based on a location
        """
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            with _ACCESS_DATABASE:
                location = db.session.query(Location).filter_by(name=location_name.lower()).first()
                if location:
                    salamanders = db.session.query(Salamander).filter_by(location_id=location.id).all()
                    if salamanders:
                        salamander_list = []
                        for salamander in salamanders:
                            number_of_images = db.session.query(SalamanderGrowth).filter_by(salamander_id=salamander.id).all()
                            last_date = db.session.query(SalamanderGrowth).filter_by(salamander_id=salamander.id).order_by(SalamanderGrowth.date.desc()).first()
                            ret = {"id": salamander.id, "sex": salamander.sex, "individuals": len(number_of_images), "species": salamander.species, "date": last_date.date}
                            salamander_list.append(ret)
                        return jsonify({"salamanders": salamander_list, 'status': 200})
                    else:
                        return jsonify({"message": "no salamanders at this location", 'status': 400})
                else:
                    return jsonify({"message": "location does not exist", 'status': 400})
        else:
            return jsonify({"message": "no access to this endpoint", 'status': 400})
