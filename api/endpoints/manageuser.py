from flask import request, jsonify
from flask_restful import Resource
from api.models.dbmodels import User
from api import db, limiter, bcrypt
from flask_jwt_extended import get_jwt_identity, jwt_required
from random import choice
from string import ascii_lowercase, digits


class AdminManageUser(Resource):
    """
    Endpoint for administrators to manage users.
    GET: Get all users in the system.
    POST: Change a users access rights. The admin can update them to admin, remove admin or remove all access rights.
    PUT: Is used if a user forgets their password. The admin can reset it, so that the user can login and change it at a
    later point.
    """

    decorators = [limiter.limit("60/minute")]

    @staticmethod
    @jwt_required
    def get():
        """
        :return: JSON object containing all users access rights, email and id
        """
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            user_list = []
            users = db.session.query(User).filter_by(accepted=1).filter(User.id != user.id).all()
            for pr_user in users:
                ret = {"email": pr_user.email, "id": pr_user.id, "admin": pr_user.admin, "accepted": pr_user.accepted}
                user_list.append(ret)
            return jsonify({"users": user_list, "status": 200})
        else:
            return jsonify({"message": "no access", "status": 400})

    @staticmethod
    @jwt_required
    def post():
        """
        param id: the updated users id
        param email: the updated users email
        param removeAdmin: removed administrator access
        param makeAdmin: grants the user administrator access
        param denyAccess: denies a user access to the system
        :return: JSON object with a confirmation message
        """
        data = request.form
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            if "id" in data and "email" in data and user_id != data['id']:
                update_user = db.session.query(User).filter_by(id=data['id']).first()

                if "removeAdmin" in data and data['email'] == update_user.email:
                    update_user.admin = False
                    db.session.commit()
                    return jsonify({"message": "user is no longer admin", "status": 200})

                if "makeAdmin" in data and data['email'] == update_user.email:
                    update_user.admin = True
                    db.session.commit()
                    return jsonify({"message": "user is now admin", "status": 200})

                if "denyAccess" in data and data['email'] == update_user.email:
                    update_user.accepted = False
                    db.session.commit()
                    return jsonify({"message": "user is downgraded", "status": 200})
            else:
                return jsonify({"message": "wrong data", "status": 400})

        else:
            return jsonify({"message": "no access", "status": 400})

    @staticmethod
    @jwt_required
    def put():
        """
        param id: the user id to the user that needs a new password
        :return: JSON object containing the new password for the user
        """
        data = request.form
        user_id = get_jwt_identity()
        user = db.session.query(User).filter_by(id=user_id).first()
        if user.admin:
            if "id" in data and user_id != data['id']:
                user = db.session.query(User).filter_by(id=data['id']).first()
                new_password = get_random_string(5)
                password_hash = bcrypt.generate_password_hash(new_password)
                user.pwd = password_hash
                db.session.commit()
                return jsonify({"new_password": new_password, "status": 200})
            else:
                return jsonify({"message": "wrong data", "status": 400})

        else:
            return jsonify({"message": "no access", "status": 400})


def get_random_string(length):
    """
    :param length: the length of the new password
    :return: a random generated password string
    """
    # choose from all lowercase letter
    letters = ascii_lowercase
    return ''.join(choice(letters + digits) for i in range(length))
