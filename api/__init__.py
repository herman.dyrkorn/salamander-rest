from flask import Flask
from flask_restful import Api
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

# flask application setup, with database, JWT, bcrypt and limiter
app = Flask(__name__)
app.config['SECRET_KEY'] = 'randomchangethisshit'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/database.db'
app.config['JWT_AUTH_USERNAME_KEY'] = 'email'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 12 * 3600
db = SQLAlchemy(app)
jwt = JWTManager(app)
bcrypt = Bcrypt(app)
limiter = Limiter(app, key_func=get_remote_address)
api = Api(app)

# import all classes that is used to create endpoints
from api.endpoints.login import Login
from api.endpoints.user import UserEndpoint
from api.endpoints.location import LocationEndpoint
from api.endpoints.manageuser import AdminManageUser
from api.endpoints.verifypassword import VerifyPassword
from api.endpoints.salamander import SalamanderEndpoint
from api.endpoints.editsalamander import EditSalamander
from api.endpoints.pendingusers import AdminPendingUsers
from api.endpoints.matchsalamander import MatchSalamander
from api.endpoints.salamanders import SalamandersEndpoint
from api.endpoints.findsalamanderinfo import FindSalamanderInfo


# add the imported classes to a route
api.add_resource(Login, "/login")
api.add_resource(UserEndpoint, "/user")
api.add_resource(LocationEndpoint, "/location")
api.add_resource(AdminManageUser, "/manageUsers")
api.add_resource(VerifyPassword, "/verifyPassword")
api.add_resource(AdminPendingUsers, "/pendingUsers")
api.add_resource(MatchSalamander, "/matchSalamander")
api.add_resource(FindSalamanderInfo, "/findSalamanderInfo")
api.add_resource(SalamanderEndpoint, "/salamander/<salamander_id>")
api.add_resource(SalamandersEndpoint, "/salamanders/<location_name>")
api.add_resource(EditSalamander, "/editSalamander", "/editSalamander/<salamander_id>/<image_id>")
