from api import db
from datetime import datetime


# creates the user table in the local database
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    pwd = db.Column(db.String(255), nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    accepted = db.Column(db.Boolean, nullable=False, default=False)


# creates the salamander table in the local database
class Salamander(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    sex = db.Column(db.String(255), nullable=False)
    species = db.Column(db.String(255), nullable=False)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), nullable=False)
    uid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    first_date = db.Column(db.DateTime, default=datetime.now)


# creates the location table in the local database
class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    radius = db.Column(db.Integer, nullable=False)


# creates the salamander_growth table in the local database
class SalamanderGrowth(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    salamander_id = db.Column(db.Integer, db.ForeignKey('salamander.id'))
    image_id = db.Column(db.Integer, nullable=False)
    date = db.Column(db.DateTime, default=datetime.now)
    weight = db.Column(db.Float, nullable=True)
    length = db.Column(db.Float, nullable=True)
