from wtforms import Form, StringField, PasswordField, validators


# form data for registering a user
class RegistrationForm(Form):
    name = StringField('name', [validators.DataRequired()])
    email = StringField('email', [validators.DataRequired()])
    password = PasswordField('password', [validators.DataRequired()])
    confirmPassword = PasswordField('confirmPassword', [validators.DataRequired()])


# form data for deleting a user
class DeleteUserForm(Form):
    email = StringField('email', [validators.DataRequired()])
